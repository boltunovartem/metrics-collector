const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const addCollectRoute = require("./collect");
const {LoggerMiddleware} = require("../api/middlewares");

class Router {
    constructor(app) {
        this.app = app;
    }

    async configure() {
        this.applyDefaultMiddlewares();
        addCollectRoute(this.app);
        this.addDefaultRoute();
    }

    applyDefaultMiddlewares() {
        if (config.allowedOrigin === "*") {
            this.app.use(cors());
        } else {
            this.app.use(cors({origin: config.allowedOrigin, credentials: true}));
        }
        this.app.use(cookieParser());
        this.app.use(bodyParser.json({limit: "10mb"}));
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(LoggerMiddleware);
    }

    addDefaultRoute() {
        this.app.use((req, res) => res.sendStatus(HttpStatus.NOT_FOUND));
    }
}

module.exports = Router;
