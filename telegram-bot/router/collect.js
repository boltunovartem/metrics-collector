const express = require("express");
const CollectApi = require("../api/collect");
const {InternalMiddleware} = require("../api/middlewares");

module.exports = (app) => {
    const collectRouter = express.Router({mergeParams: true});
    collectRouter.use(InternalMiddleware);
    collectRouter.route("/metrics").put(CollectApi.saveMetrics);

    app.use(`${config.apiRoot}/collect`, collectRouter);
    logger.info("Add metrics router");
};
