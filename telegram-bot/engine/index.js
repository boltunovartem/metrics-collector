const express = require("express");
const mongoose = require("mongoose");
const Router = require("../router");

class Engine {
    constructor() {
        this.app = null;
        this.router = null;
        this.db = null;
    }

    connectMongoDB = async () => {
        try {
            logger.log(`Connect to MongoDB (${config.mongoUrl})`);
            await mongoose.connect(config.mongoUrl, {
                useNewUrlParser: true,
                family: 4,
                connectTimeoutMS: 2000,
                autoReconnect: false,
                useUnifiedTopology: true,
                useCreateIndex: true,
            });
            logger.info("MongoDB connected");
            this.db = mongoose.connection;
        } catch (e) {
            logger.error("MongoDB connect failed");
            logger.error(e.message);
            process.exit(0);
        }
    };

    start = async () => {
        this.app = express();
        this.router = new Router(this.app);
        await this.router.configure();

        this.runServer();
    };

    runServer = () => {
        this.app.listen(config.port, () => {
            logger.info(`Start server on port ${config.port}`);
        });
    };
}

module.exports = Engine;
