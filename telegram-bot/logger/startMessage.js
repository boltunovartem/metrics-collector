const Table = require("cli-table");
const packageJson = require("../package.json");

const table = new Table({
    chars: {
        top: "═",
        "top-mid": "╤",
        "top-left": "╔",
        "top-right": "╗",
        bottom: "═",
        "bottom-mid": "╧",
        "bottom-left": "╚",
        "bottom-right": "╝",
        left: "║",
        "left-mid": "╟",
        mid: "─",
        "mid-mid": "┼",
        right: "║",
        "right-mid": "╢",
        middle: "│",
    },
    colAligns: ["right", "left"],
});

const helloStr = `Metrics Bot`;
const authorStr = `(c) ${packageJson.author.name}`;
table.push(
    {App: [helloStr]},
    {Version: [`v${packageJson.version}`]},
    {Author: [authorStr]},
    {"E-mail": ["mailto:" + packageJson.author.email]},
    {Git: ["https://gitlab.com/boltunovartem/metrics-collector"]}
);

module.exports = table.toString();
