const moment = require("moment");
const startMessage = require("./startMessage");

class Logger {
    constructor() {
        console.clear();
        console.log(startMessage);
    }

    _log(message, type) {
        const currentDate = moment().format("DD.MM.yyyy HH:mm.ss");
        console[type](`[${currentDate}]`, `(${type})`, message);
    }

    log = (message) => this._log(message, "log");
    info = (message) => this._log(message, "info");
    warn = (message) => this._log(message, "warn");
    error = (message) => this._log(message, "error");
}

module.exports = new Logger();
