const metricsUsecases = require("../internal/metrics/usecases");
const serversUsecases = require("../internal/servers/usecases");
const Monitor = require("../internal/monitor/Monitor");

const {sendInternalServerError, sendResponse} = require("./common");

const saveMetrics = async (req, res, next) => {
    try {
        const {ip, name, cpu, ram, disk} = req.body;
        const serverData = {ip, name};
        const server = await serversUsecases.getServer(serverData);

        await Monitor.handleGetMetrics(server, {cpu, ram, disk});

        await metricsUsecases.saveMetric(server, {cpu, ram, disk});

        sendResponse(HttpStatus.OK, null, res);
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

module.exports = {
    saveMetrics,
};
