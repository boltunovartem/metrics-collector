const sendUnauthorizedError = (res) => {
    res.status(HttpStatus.UNAUTHORIZED).send({
        type: "error",
        message: "Unauthorized request",
    });
};

const InternalMiddleware = (req, res, next) => {
    const secret = req.headers["internal-secret"];
    if (secret !== config.internalSecret) {
        return sendUnauthorizedError(res);
    }
    next();
};

const LoggerMiddleware = (req, res, next) => {
    const startTime = new Date();
    res.on("finish", () => {
        const requestTime = new Date() - startTime;
        logger.info(
            `${req.protocol.toUpperCase()} [${req.method}] ${req.url} - ${res.statusCode} ${requestTime}ms from ${req.ip}`
        );
    });
    return next();
};

module.exports = {
    LoggerMiddleware,
    InternalMiddleware,
};
