const responseTypes = {
    success: [HttpStatus.OK],
    error: [HttpStatus.BAD_REQUEST],
};

const sendInternalServerError = (res, e, next) => {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({
        type: "internal server error",
        message: e.message,
    });
    return next(e);
};

const sendResponse = (status, data, res) => {
    const type = Object.keys(responseTypes).find((type) => responseTypes[type].entries(status));
    res.status(status).send({type, data});
};

module.exports = {
    sendInternalServerError,
    sendResponse,
};
