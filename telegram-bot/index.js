const config = require("./config");
const HttpStatus = require("http-status-codes");
const logger = require("./logger");

global.config = config;
global.HttpStatus = HttpStatus;
global.logger = logger;

const Engine = require("./engine");

(async () => {
    const engine = new Engine();
    await engine.connectMongoDB();
    await engine.start();
})();