const ServerModel = require("./server");

const addServer = async (data) => {
    return ServerModel.create(data);
}

const getServer = async (data) => {
    const {id, name, ip} = data;

    const server = await ServerModel.findOne({
        $or: [
            {_id: id},
            {$and: [{name}, {ip}]}
        ]
    });

    if (!server) {
        return addServer(data);
    }

    return server;
}

module.exports = {
    addServer,
    getServer
}
