const mongoose = require("mongoose");

const ServerSchema = mongoose.Schema({
    ip: {type: String, required: true},
    name: {type: String, required: true}
});

module.exports = mongoose.model("Server", ServerSchema);