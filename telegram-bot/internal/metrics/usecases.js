const MetricModel = require("./metric");
const moment = require("moment")

const saveMetric = async (server, {cpu, ram, disk}) => {
    await MetricModel.create({
        server, cpu, ram, disk,
        createdAt: moment().toDate()
    });
};

const getMetricsByServer = async (server, options = {}) => {
    const {range} = options;
    const query = {server};

    const now = moment();
    if (["day", "week", "month"].includes(range)) {
        query.createdAt = {
            $lte: now.toDate(),
            $gte: now.subtract( 1, range).toDate(),
        }
    }
    return MetricModel.find(query);
}

const clearData = async (range) => {
    const query = {};
    const now = moment();

    if (["day", "week", "month"].includes(range)) {
        query.createdAt = {
            $lte: now.toDate(),
            $gte: now.subtract( 1, range).toDate(),
        }
    }
    await MetricModel.deleteMany(query)
}

module.exports = {
    saveMetric,
    getMetricsByServer,
    clearData
}
