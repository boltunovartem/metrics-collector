const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;

const MetricsSchema = mongoose.Schema({
    server: {type: ObjectId, ref: "Server", required: true},
    cpu: {type: Number, required: true, default: 0},
    disk: {type: Number, required: true, default: 0},
    ram: {type: Number, required: true, default: 0},
    createdAt: {type: Date, required: true, default: new Date()}
});

MetricsSchema.methods.toDTO = async function () {
    await this.populate("server").execPopulate();
    return {
        server: {
            name: this.server.name,
            ip: this.server.ip
        },
        cpu: this.cpu,
        disk: this.disk,
        ram: this.ram,
        createdAt: this.createdAt
    }
}

module.exports = mongoose.model("Metric", MetricsSchema);