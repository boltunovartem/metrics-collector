const telegram = require("../telegram");

class Monitor {
    constructor() {
        this.servers = new Map();
    }

    handleGetMetrics = async (server, metrics) => {
        const {_id, ip, name} = server;
        const id = _id.toString();

        if (this.servers.has(id)) {
            clearTimeout(this.servers.get(id));
        } else {
            await this.alert(`Server "${name}" [${ip}] has been registered in monitor`);
        }

        const timeout = setTimeout(async () => {
            await this.alert(`Server "${name} [${ip}]" is down`);
        }, config.checkInterval * 1000);

        this.servers.set(id, timeout);
        telegram.updateActiveServers(Array.from(this.servers.keys()));
        const warning = this.checkMetrics(metrics);
        if (warning) await this.alert(`${name} [${ip}] ${warning}`)
    }

    checkMetrics = ({cpu, ram, disk}) => {
        if (cpu >= 80) return `CPU usage is to high: ${cpu}%`;
        if (ram >= 80) return `RAM usage is to high: ${ram}%`;
        if (disk >= 96) return `DISK usage is to high: ${disk}%`;
    };


    alert = async (message) => {
        if (config.useTelegram) {
            return telegram.sendMessage(message);
        }
        logger.info(message);
    }

    getServers = () => {
        return this.servers.keys();
    }
}

module.exports = new Monitor();