process.env.NTBA_FIX_319 = 1;

const TelegramBot = require("node-telegram-bot-api");
const moment = require("moment");
const ServerModel = require("../servers/server");
const metricsUsecases = require("../metrics/usecases");
const fs = require("fs");
const js2xmlparser = require("js2xmlparser");

class Telegram {
    constructor() {
        if (!config.useTelegram) return;
        this.activeServers = [];
        this.bot = new TelegramBot(config.telegramToken, {polling: true});
        this.registerCommand("help", () => this.sendHelpMessage(config.telegramChatId))
        this.sendHelpMessage(config.telegramChatId);
        this.bot.on("callback_query", this.handlerCallbackQuery)
    }

    sendMessage = async message => {
        await this.bot.sendMessage(config.telegramChatId, message);
    };

    sendReport = async path => {
        await this.bot.sendDocument(config.telegramChatId, path);
    }

    sendHelpMessage = async (chatId) => {
        await this.bot.sendMessage(chatId, `Привет! Это бот для сбора метрик с серверов, на которых установлен сборщик метрик.\nТы можешь управлять мной используя кнопки под сообщением.`,
            {
                "reply_markup": {
                    "inline_keyboard": [
                        [{text: "Получить отчет", callback_data: "report_button"}],
                        [{text: "Получить список серверов", callback_data: "get_servers_list"}],
                        [{text: "Очистить данные метрик", callback_data: "clear_metrics"}],
                        [{text: "Получить помощь", callback_data: "get_help"}],
                    ]
                }
            });
    }

    handlerCallbackQuery = async (ctx) => {
        switch (ctx.data) {
            case "report_button":
                await this.bot.sendMessage(ctx.message.chat.id, "Выберите сервер", {
                    "reply_markup": {
                        "inline_keyboard": [
                            ...(this.activeServers.map(s => ([{text: `${s.name} [${s.ip}]`, callback_data: `server:${s._id.toString()}`}]))),
                            [{text: "Все сервера", callback_data: "server:all"}]
                        ]
                    }
                });
                break;
            case "get_help":
                await this.sendHelpMessage(ctx.message.chat.id);
                break;
            case "get_servers_list":
                const serversString = this.activeServers.map((s, i) => `${i+1}. <i>${s.name}</i>\nIPv4: ${s.ip}`).join("\n");
                await this.bot.sendMessage(ctx.message.chat.id, `<b>Список серверов:</b>\n${serversString}\n`, {parse_mode: "HTML"});
                break;
            case "clear_metrics":
                await this.bot.sendMessage(ctx.message.chat.id, `Временной период очистики данных:`, {
                    "reply_markup": {
                        "inline_keyboard": [
                            [{text: "Очистить данные за день", callback_data: `clear:day`}],
                            [{text: "Очистить данные за неделю", callback_data: `clear:week`}],
                            [{text: "Очистить данные за месяц", callback_data: `clear:month`}],
                            [{text: "Очистить данные за все время", callback_data: `clear:all`}],
                        ]
                    }
                });
                break;
            default:
                await this.execCommand(ctx);
        }
    }

    execCommand = async (ctx) => {
        const command = ctx.data.split(":")[0];
        switch (command) {
            case "server":
                const id = ctx.data.split(":")[1];
                const server = this.activeServers.find(s => id === s._id.toString());
                if (server || id === "all") {
                    await this.bot.sendMessage(ctx.message.chat.id, `Временной период отчета для ${server ? server.name : "всех серверов"}`, {
                        "reply_markup": {
                            "inline_keyboard": [
                                [{text: "Отчет за день", callback_data: `type:${id}:day`}],
                                [{text: "Отчет за неделю", callback_data: `type:${id}:week`}],
                                [{text: "Отчет за месяц", callback_data: `type:${id}:month`}],
                                [{text: "Отчет за все время", callback_data: `type:${id}:all`}],
                            ]
                        }
                    });
                } else {
                    logger.error({ctxData: ctx.data, servers: this.activeServers});
                    await this.sendMessage("Server not active")
                }
                break;
            case "type":
                const srvId = ctx.data.split(":")[1];
                const timeRange = ctx.data.split(":")[2];

                await this.bot.sendMessage(ctx.message.chat.id, `Выберите формат отчета`, {
                    "reply_markup": {
                        "inline_keyboard": [
                            [{text: "Текст", callback_data: `report:text:${srvId}:${timeRange}`}],
                            [{text: "JSON", callback_data: `report:json:${srvId}:${timeRange}`}],
                            [{text: "XML", callback_data: `report:xml:${srvId}:${timeRange}`}],
                        ]
                    }
                });
                break;
            case "report":
                const [, type, serverId, range] = ctx.data.split(":");
                const isAllReport = serverId === "all";
                const srv = isAllReport ?
                    this.activeServers.map(s => s._id.toString()) :
                    this.activeServers.find(s => serverId === s._id.toString());

                const metrics = await metricsUsecases.getMetricsByServer(srv, {range});
                const dto = await Promise.all(metrics.map(async metric => await metric.toDTO()));

                if (metrics) {
                    const name = `${isAllReport ? "all" : srv.name}-${moment().format("DD.MM.YY-HH:mm:ss")}`;
                    await this.sendMetrics(type, name, dto);
                } else {
                    await this.sendMessage(`Для ${srv.name} метрик нет`);
                }
                break;
            case "clear":
                const clearTimeRange = ctx.data.split(":")[1];
                await metricsUsecases.clearData(clearTimeRange);
                await this.sendMessage(`${ctx.from.first_name}, метрики успешно очищены!`);
                break;
            default:
                await this.sendMessage(`Команда не найдена, воспользуйтесь командой /help`);
        }
    }

    sendMetrics = async (type, name, dto) => {
        let path;
        try {
            switch (type) {
                case "json":
                    path = config.filePath + `${name}.json`;
                    fs.writeFile(path, JSON.stringify(dto), () => {
                        this.sendReport(path);
                    })
                    break;
                case "text":
                    const text = dto.map(d => `[${d.createdAt.toISOString()}] ${d.server.name} - ${d.server.ip}: CPU = ${d.cpu} RAM = ${d.ram} DISK = ${d.disk}`);
                    const data = text.join("\n");
                    path = config.filePath + `${name}.txt`;
                    fs.writeFile(path, data, () => {
                        this.sendReport(path);
                    })
                    break;
                case "xml":
                    const xmlData = js2xmlparser.parse("result", dto);
                    path = config.filePath + `${name}.xml`;
                    fs.writeFile(path, xmlData, () => {
                        this.sendReport(path);
                    })
            }
        } catch (e) {
            logger.error(e);
        }
    }

    updateActiveServers = async (servers) => {
        this.activeServers = await ServerModel.find({_id: {$in: servers}});
    }

    registerCommand = (command, cb) => {
        this.bot.onText(new RegExp(`/${command}`), async (msg) => {
            await cb(msg);
        });
    };
}

module.exports = new Telegram();
