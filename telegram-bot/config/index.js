const {env} = process;
module.exports = {
    allowedOrigin: env.ALLOWED_ORIGIN || "*",
    port: env.PORT || 4000,
    mongoUrl: env.MONGO_URL || "mongodb://localhost:27030/metrics-collector",
    internalSecret: env.INTERNAL_SECRET || "internal_secret",
    checkInterval: 10,
    apiRoot: "/telegram-bot/v1",
    useTelegram: env.USE_TELEGRAM || false,
    telegramToken: "1725248127:AAFvl2tSlVQ8Cv9XjVToIYoCcEUSRBOS_-Q",
    telegramChatId: "-570263678",
    filePath: '/data/'
};
