const {env} = process;

module.exports = {
    cpuWarningLevel: env.CPU_WARNING_LEVEL || 90,
    ramWarningLevel: env.RAN_WARNING_LEVEL || 90,
    diskWarningLevel: env.DISK_WARNING_LEVEL || 96,
    apiPort: env.BOT_PORT || 4000,
    apiHost: env.BOT_HOST || "http://localhost",
    checkInterval: env.CHECK_INTERVAL || 5, // seconds
    serverName: env.SERVER_NAME || env.HOST,
    internalSecret: env.INTERNAL_SECRET || "internal_secret",
    apiRoot: env.API_ROOT || "/telegram-bot/v1"
};
