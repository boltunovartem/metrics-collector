const systemInformation = require("nodejs-system-info");
const mem = require("node-os-utils").mem;
const disk = require("diskusage");
const fetch = require("node-fetch");
const moment = require("moment");
const config = require("./config");
const {networkInterfaces, platform, hostname} = require("os");
const path = platform() === "win32" ? "c:" : "/";

const SERVER_IP = Object.values(networkInterfaces())
    .filter(u => u.find(i => i.family === "IPv4" && i.netmask === "255.255.255.0"))[0]
    .find(u => u.family === "IPv4").address;

const systemMonitor = new systemInformation(100);
systemMonitor.get(["cpu"]);

const formatUsage = usage => {
    return usage ? parseFloat(usage.split("%")[0]) : 0;
};

const log = (type, message) => {
    const currentDate = moment().format("DD.MM.yyyy HH:mm.ss");
    console[type](`[${currentDate}]`, `(${type.toLowerCase()})`, message);
}

const getDiskUsage = async () => {
    const {free, total} = await disk.check(path);
    return ((1 - free / total) * 100).toFixed(2);
};

const samples = 5;

const statQueues = {};

const initQueue = async () => {
    statQueues.cpu = Array(samples).fill(+formatUsage(systemMonitor.get(["cpu"]).CPU_Usage));
    statQueues.ram = Array(samples).fill(100 - (await mem.info()).freeMemPercentage);
    statQueues.disk = Array(samples).fill(await getDiskUsage());
};

const updateStat = (stat, value) => {
    statQueues[stat].shift();
    statQueues[stat].push(value);
    return statQueues[stat].reduce((acc, v) => acc + v / samples, 0);
};

const checkServerStatus = async () => {
    const {CPU_Usage} = systemMonitor.get(["cpu"]);
    const ramInfo = await mem.info();

    const cpu = updateStat("cpu", +formatUsage(CPU_Usage)).toFixed(2);
    const ram = updateStat("ram", 100 - ramInfo.freeMemPercentage).toFixed(2);
    const disk = updateStat("disk", +(await getDiskUsage())).toFixed(2);

    const {apiRoot, apiPort, apiHost} = config;
    const apiUrl = `${apiHost}:${apiPort}${apiRoot}/collect/metrics`;

    const body = JSON.stringify({
        ip: SERVER_IP,
        name: config.serverName,
        cpu,
        ram,
        disk
    });

    log("info", apiUrl);
    try {
        await fetch(apiUrl, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Internal-Secret": config.internalSecret
            },
            body
        });
    } catch (e) {
        log("error", `Failed to send message: ${e.message}`);
    }
};

if (!config.serverName) {
    config.serverName = hostname();
}


setTimeout(initQueue, config.checkInterval * 500);
setInterval(async () => {
    await checkServerStatus();
}, config.checkInterval * 1000);
log("info", "Server was started");

